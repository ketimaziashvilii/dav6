package com.example.davaleba6.Fragments.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.davaleba6.Fragments.FragMe
import com.example.davaleba6.Fragments.FragNote
import com.example.davaleba6.Fragments.FragPhoto

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle): FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {

     return when (position) {

            0 -> {
                FragNote()
            }

            1 -> {
                FragPhoto()
            }

            2 -> {

                FragMe()
            }

            else->{

                Fragment()
            }
        }

    }

}
